import path from 'path'
import webpack from 'webpack'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
// import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin'

import { clientConfiguration } from 'universal-webpack'
import settings from './universal-webpack-settings.json' assert { type: 'json' }
import baseConfiguration from './webpack.config.js'

const configuration = clientConfiguration(baseConfiguration, settings, {
  // Extract all CSS into separate `*.css` files (one for each chunk)
  // using `mini-css-extract-plugin`
  // instead of leaving that CSS embedded directly in `*.js` chunk files.
  development: false,
  useMiniCssExtractPlugin: true
})

configuration.devtool = 'source-map'

// Minimize CSS.
// https://github.com/webpack-contrib/mini-css-extract-plugin#minimizing-for-production
configuration.optimization = {
  minimizer: [
    new TerserPlugin({
      parallel: true
    }),
    new CssMinimizerPlugin()
  ]
};

configuration.plugins.push(
  // Clears the output folder before building.
  new CleanWebpackPlugin(),

  // // Shows the resulting bundle size stats.
  // // https://github.com/webpack-contrib/webpack-bundle-analyzer
  // new BundleAnalyzerPlugin({
  //   // The path is relative to the output folder
  //   reportFilename : '../bundle-stats.html',
  //   analyzerMode   : 'static',
  //   openAnalyzer   : false
  // })
)

export default configuration
